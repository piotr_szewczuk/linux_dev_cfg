# Script for settingup development envirement on linux just how I like it

#! /bin/bash

echo '*** -> system setup start'


#=========================================================

## installing packages

echo 'GIT installation'
sudo apt-get install git

echo 'kdiff3 installation'
sudo apt-get install kdiff3

echo 'gitkraken installation'
sudo snap install gitkraken

echo 'g++ installation'
sudo apt-get install g++

echo 'cmake installation'
sudo apt-get install cmake

echo 'clang-format installation'
sudo apt-get install clang-format

echo 'qtcreator installation'
sudo apt-get install qtcreator

echo 'xclip installation'
sudo apt-get install xclip

echo 'fonts-powerline installation'
sudo apt-get install fonts-powerline

echo 'atom installation'
sudo apt install curl
curl -sL https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
sudo apt-get update
sudo apt-get install atom

#==========================================================

## setting up dev directory

cd ~
mkdir --parents --verbose dev/system_setup
mkdir --parents --verbose dev/projects


#==========================================================

## cloning linux_dev_settings repo

echo 'clonning cfg repo'
cd ~/dev/system_setup
git clone https://piotr_szewczuk@bitbucket.org/piotr_szewczuk/linux_dev_cfg.git


#==========================================================

## git setup

echo 'GIT setup'
ln linux_dev_cfg/git_cfg/.gitconfig ~


#==========================================================

## aliases

echo 'aliases setup'
ln linux_dev_cfg/linux_cfg/.aliases ~
echo '# my settings

if [ -f ~/.aliases ]; then
    . ~/.aliases
fi' >> ~/.bashrc

#==========================================================

## console printout

echo 'terminal printout setup'
cat linux_dev_cfg/linux_cfg/prompt >> ~/.bashrc


#==========================================================

## gnome-terminal setup

echo 'gnome-terminal setup'
mkdir --parents --verbose ~/.config/autostart
ln linux_dev_cfg/linux_cfg/gnome-terminal.desktop ~/.config/autostart


#==========================================================

## QtCreator settings

echo 'qtcreator setup'
mkdir --parents --verbose ~/.config/QtProject/qtcreator/styles
ln linux_dev_cfg/qt_cfg/styles/creator-dark_copy.xml ~/.config/QtProject/qtcreator/styles

mkdir --parents --verbose ~/.config/QtProject/qtcreator/codestyles/default
ln linux_dev_cfg/qt_cfg/codestyles/default/Global.xml ~/.config/QtProject/qtcreator/codestyles/default


#==========================================================

## gedit setup

gsettings set org.gnome.gedit.preferences.editor scheme 'oblivion'
gsettings set org.gnome.gedit.preferences.editor auto-indent true
gsettings set org.gnome.gedit.preferences.editor tabs-size 4
gsettings set org.gnome.gedit.preferences.editor right-margin-position 100
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
gsettings set org.gnome.gedit.preferences.editor bracket-matching true
gsettings set org.gnome.gedit.preferences.editor auto-save true
gsettings set org.gnome.gedit.preferences.editor display-right-margin true


#==========================================================

## dock setup

gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 20
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position 'RIGHT'

#==========================================================

## system restart

read -p 'reboot [y/n] > ' REBOOT
if [[ "${REBOOT}" == "n" || "${REBOOT}" == "N" ]]; then
	exit 0
fi
sudo reboot
